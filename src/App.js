import React, { useEffect, useState } from 'react';
import axios from './axios';
import Questions from './Questions';
import './App.css';

function App() {

  const [que ,setque]=useState([]);
  const [len ,setlen]=useState(0);
  useEffect(()=>{
    axios.get('/questions')
    .then(response=>{
      setque(response.data)
      setlen(response.data.length)
    })
   
  },[]);

  return (    
    <div className="App" > 
      <form action="http://127.0.0.1:9000/done" method="post">
         <Questions question={que}/>
        <input type="hidden" value={len} name="quelength"/>
        <input type="submit" value="submit"/>
       </form>
    </div>
  );
}

export default App;
